# EkBattleBoff

EkBattleBoff is a windows application for simulating battles in the Elemental Kingdoms mobile card game.
It uses the [ek-battlesim](https://bitbucket.org/crystark/ekbattlesim), by Crystark.

### Downloads

 - [Latest version](https://bitbucket.org/crystark/ekbattleboff/downloads/) - 0.1

### Usage

To use EkBattleBoff, simply download and extract all files from the zip. Then double-click the EkBattleBoff.exe to start the application.

### License

Permission to use, copy, modify, and/or distribute this software for any purpose with or without fee is hereby granted.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES WITH REGARD TO THIS SOFTWARE 
INCLUDING ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE 
FOR ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES WHATSOEVER RESULTING FROM 
LOSS OF USE, DATA OR PROFITS, WHETHER IN AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, 
ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.