CHANGELOG
====================

1.1.2 - 2016-03-15
--------------------
    
- **Bugfixes**
    - Silence is now correctly removed after the card's attack.
    - Fixed `Spine` not hitting for full damage when a card has less health than it was hit for.
    - Fixed `Charging` so it activates on the card's turn. Use `QS_Charging` for the old behavior.

1.1.1 - 2016-03-03
--------------------
    
- **Features**
    - Silence's behavior has been changed to follow the fix EK did recently.
    - New skills: `Advanced Rejuvenation`, `Charging`, `City Defense`, `Cleanse`, `Jungle`, `Spell Reduction`
    - The EW decks are now bundled into the sim. You can call `-ew 'Goddess of Order'` instead of using a file as input. File input still works.
    
- **Bugfixes**
    - [GUI] Fixed untimely file change notifications in deck editor.
    - [Test] Fixed the launch condition ability tokens to keep an activated skill active until the card dies.
    
- **Tech changes**
    - [GUI] Empty lines won't be commented when using `CTRL+Q`

1.1.0 - 2016-02-18
--------------------
    
- **Features**
    - Added a basic *BETA* text-based deck editor with syntax coloring and shortcuts. Try `CTRL+Q`, `CTRL+D` and of course `CTRL+S` !
    - New deck option `--bypass-validation` allows to skip game-rule-specific validations (Card limit, Rune limit, ...).
    - New skills: `Diana's Touch`, `Imperius`
    
- **Bugfixes**
    - Fixed a rare crash occurring with `on hit` skills when a card was killed while using them.
    - Fixed the card limit validation with full description using the same name as a limited card.

1.0.4 - 2016-01-30
--------------------
    
- **Features**
    - New skill implemented: `Touch of Vampire`
    - The sim now prints the number of cards in your deck next to the cost.
    
- **Bugfixes**
    - `Time Reverse` now only works once per battle.
    
- **Tech changes**
    - `Summon Weapon` renamed `Violent Storm`

1.0.3 - 2016-01-26
--------------------
    
- **Features**
    - [GUI] console output will now be truncated if over 200k characters to avoid output lag.
    - [GUI] demon description is now always shown
    - The sim now validates that the user decks contains no conflicting cards or cards used over their limit.
    
- **Bugfixes**
    - [GUI] ignore saved window positioning if off screen; 
    - Dodge now uses it's own RNG to comply with in game observations.
    - Fixed a rare bug with Clean Sweep
    - Fixed a rare bug with Crazy
    - Fixed `Life Link` to work only on attacks


1.0.2 - 2016-01-18
--------------------
    
- **Features**
    - New skills implemented: `Execution`, `Life Link`
    
- **Bugfixes**
    - Fixed `Break Ice` vs Bosses
    - Fixed `Drain Life` vs Immunes and Bosses
    - Fixed `Summon Weapon` vs Player

1.0.1 - 2016-01-07
--------------------
    
- **Features**
    - New cards in DB: `Dinotoad`, `Feral Stalker`, `Frost Goliath`, `Moon Druid`, `Storm Spirit`, `Strange Visitor`, `The Fury`
    - New skills implemented: `Break Ice`, `Brutal Claw`, `Drain Life`, `Eruption`, `Land of Lava`, `Machinegun`, `Road of Ashes`, `Storm Force`
    
- **Bugfixes**
    - Issue #193 - Fixed `Flash` and `Blind` getting removed too soon.
    - Fixed `Salvo` targets
    - Fixed `Dead Roar` targets
    - Fixed `Cold Blood` against `Last Chance`
    - Issue #188 - [GUI] Added checks to avoid exceptions when clearing the sim number field
    - Issue #189 - [GUI] Fixed unexpected log when deleting a custom demon.
    - Issue #190 - [GUI] Fixed the exit menu item to properly save the DBroperly save the DB
    
- **Tech changes**
    - `openwindow.bat` renammed to `!openwindow.bat` to be first in the list alphabetically
    - Composite skills are adaptive depending on the values in the DB.

1.0.0 - 2015-12-24
--------------------

**Merry Christmas and Happy New Year to all of you.**
    
- **Features**
    - **Added first basic GUI! Use `ek-battlesim-gui.exe` to launch it. Here are the headlines:**
        - Only supports DI sim for now. (Use the cli for the currently unsupported features)
        - Doesn't support deck creation (you still have to create deck using text file)
        - Allows to create (and save) custom demons.
        - Remebers the demon-deck association.
        - Remebers the last used parameters (but not the results).
        - A bit faster than the CLI version as it doesn't have to load everything on each run.
        - For the keyboard addicts, press enter to launch the sim in the CLI.
        - Untested on mac/linux. Should work with `javaw -cp ek-battlesim.exe crystark.ek.gui.MainApp`
    - New skills implemented: `Summon Weapon`, `Cold Blood`
    - New demon available for DI Sim: `Azathoth`
    
- **Bugfixes**
    - Fixed `Melody` to use `Reflection 9`...

0.6.21 - 2015-12-16
--------------------
    
- **Features**
    - Updated `Acrobatics` with it's dodge part.
    
- **Bugfixes**
    - Fixed `Melody` to use `Reflection 5`
    - Fixed `Salvo` targets
    - Fixed `Dead Roar` targets

0.6.20 - 2015-12-05
--------------------

**Note: `-uldb` may not work as expected. You may want to delete any local db you're using.**

- **Features**
    - New skills implemented: `Soul Devour`, `Acrobatics`, `Frenzy`, `Original Blessing`, `Melody`
    - Updated the DB with the latest cards: `Arctic Whale`, `Bastet`, `Centaur Warrior`, `Evil Mantis`
    
- **Bugfixes**
    - Fixed skills: `Soul Imprison`, `Suppressing`

0.6.19 - 2015-11-27
--------------------

- **Features**
    - **You can now specify hydras using a shorter notation**. If you don't specifiy a full name (`h3` instead of `h3 curse`) then all hydras of that category will be selected.
    - Added missing H1 to hydra pool
    - LoA Liliths have been added to the hydra pool. Use `-hy "l"` to sim against all Liliths.
    - New skills implemented: `Soul Imprison`, `Spiritual Voice`, `Suppressing`
    - New LoA/AM skills implemented: `AM Healing`, `AM Regeneration`
    - New LoA/AM rune implemented: `AM Renaissance`
    - Tests: Added new card tokens to force a conflict type (demon son, zodiac, ...)
    - Tests: Added new skill modifiers to force an launch condition on a skill (zodiac cards synergy)
    
- **Bugfixes**
    - Fixed `Crazy`
    - Should now ignore the BOM character at the beginning of a line (invisible character that could break the file parsing)
    - Nothing should now prevent the deck extraction tool from advancing to the next deck.
    - The generic `Summon` skill now activates only once per life to comply with LoA
    - Fixed `Psychic Master` and `Psychic Master Hand` crashing the sim

0.6.18 - 2015-11-16
--------------------

- **Features**
    - New skill implemented: `Summon Dragon II`
    
- **Bugfixes**
    - Missing skills should not throw an error anymore when extracting RM decks
    - Fixed `Double Attack` regarding dodge

0.6.17 - 2015-11-08
--------------------

- **Bugfixes**
    - Fixed `Double Attack`
    
- **Tech changes**
    - The sim is now up to 10% faster.

0.6.16 - 2015-11-03
-------------------- 

- **Features**
    - Versus mode now will print your relative initiative score.
    - New skill implemented: `Psychic Master Hand` (Takes the card from the Hand)

- **Bugfixes**
    - `Psychic Master` now takes the card from the deck as per the description.
    - If RM deck extraction fails for one deck, it will now continue with the next and print warnings in the end for each failed deck.

0.6.15 - 2015-10-28
-------------------- 

- **Features**
    - New skills implemented: `Caesar's Strike`, `Magic Arrays`, `Psychic Master`, `Spell Mark`, `Water Shield`
    - Updated DB contains the following new cards: `Giant Starfish`, `Goblin Chieftain`, `Infernal Jester`

- **Bugfixes**
    - Fixed how desperation skills trigger.
    - Fixed blind.

0.6.14 - 2015-10-23
--------------------

- **Features**
    - New skills implemented: `Blind`, `Devour`, `Double Attack`, `Flash`, `Salvo`, `Sensitive`

- **Bugfixes**
    - Fixed cards attacking the player when they were killed by reflection
    - Fixed `Last Chance` (`Dying Strike` will trigger twice)
    - Custom summon skill now works with simple parent card description

0.6.13 - 2015-10-20
--------------------

- **Features**
    - Versus mode now will print the following ratios: `Wins by Health`, `Wins by Cards`, `Losses by Health` and `Losses by Cards`
    - Sim outputs now print valid full card descriptions for the player decks.
    - EW sim output changes:
        - DPF isn't shown as a ratio anymore as the demon's HP vary
        - Prints if skip-shuffle is used
        - Prints if wait-to-play is used
    - Extract tools and FoH sim do not rely on server DB anymore but only on local DB. This is to avoid unexpected errors due to server-side updates. If needed, server DB can be used by updating local DB with `-uldb`.

- **Bugfixes**
    - Fixed roars when combined with plague / weaken
    - `--wait-to-play=#` minimum set to 2 (1 is actually playing normally)

0.6.12 - 2015-10-17
--------------------

- **Features**
    - Added the new deck option `--wait-to-play=#` that makes the sim wait until he hand has reached `#` before playing all the cards at once.
    - KW honor per win is now more precise and prints a range.
    - KW sim max battles can now be configured with the `--limit #` option.
    - KW sim max battles' default reduced from 1000 to 500 **Note: this will affect you sim results**

- **Bugfixes**
    - Fixed mana corruption with silenced cards
    - The update checker won't take up sim time from now on.
    - The sim will now ignore hidden files
    
- **Tech changes**
    - The sim is now up to 25% faster !

0.6.11a - 2015-10-14
--------------------

- **Bugfixes**
    - Fixed cancer's description when extracting decks.

0.6.11 - 2015-10-13
--------------------

- **Features**
    - The KW sim now shows approximate honor per win
    - New skills implemented: `Double Wings`

- **Bugfixes**
    - Useless skills (like Cancer's new skill) now count as a skill regarding reanimation. 
    
- **Tech changes**
    - `Orison` renamed to `Bless`

0.6.10a - 2015-10-10
--------------------

- **Bugfixes**
    - Fixed RM and FoH deck extraction

0.6.10 - 2015-10-10
--------------------

**Note: All the new cards will now work properly. New cards that were added in the previous version:**

 `Angra Mainyu`, `Ghoulmammoth`, `Infernal Simian`, `Virgo`, `Volcano Demon`, `Vulcan`, `Giant Frost Toad`, `Medusa`, `Prophet`, `Thunder Warhawk`, `Voyager`, `Scavenger Griffin` 

- **Features**
    - The sim now warns about unimplemented skills and ignores those when running.
    - New skills implemented: `Apocalypse`, `Cerberus`, `Crazy`, `Demon Skin`, `Diana's Protection`, `Field of Chaos`, `Plague Blast`, `Plague Maid`, `Spine`

- **Bugfixes**
    - Fixed how reanimation works regarding Cancer
    
- **Tech changes**
    - `Critical Counterattack` renamed to `Dying Strike`
    - `Protection` renamed to `Block`
    - `Explosion` renamed to `Asura's Flame`

0.6.9 - 2015-10-08
--------------------

**Note: This version includes the latest EK database but the new skills have not been implemented. Use of those skills and related cards may result in a crash. Those will soon be fixed.**

- **Features**
    - The sim will now notify you if an update is available.
    - New implemented skill: `Inspire`
    - Issue #153: The sim now allows to mix custom skill launch type (QS_ or D_) with IGN skill (e.g. Prayer 7). So now you can write `D_Prayer 7` instead of `Desperation: Prayer 7` or `D_Prayer:280`
    - Will now output a clear error when a skill is missing it's value 
    - VS mode outputs some more info on oponnent decks

- **Bugfixes**
    - *Fixes crashes due to latest EK Updates*
    - Fixed `Death Marker`'s effect not being triggered on silenced cards
    - Fixed `Reflection` when paired with `Immunity`
    - Fixed invalid error when using high number values
    - Tiny accuracy improvement
    
- **Tech changes**
    - Removed `Sacred Shield` in favor of `Divine Shield` (duplicate)
    - Renamed `Am Retractation` to `Time Reverse`
    - Renamed `Triple Mana Corruption` to `Mana Burn`
    - Removed old EK demons and AM demons from the database. You can still sim them using full demon description in the command line.
    - Rune `Am Envol` now requires a value and can't work with levels. e.g. `Am Envol:200`


0.6.8 - 2015-09-22
--------------------

- **Features**
    - New Skills for other games and/or test purpose:
        - Issue #147: `Divine Shield`: The first physical attack against this card will be nullified.
        - Issue #113: `Summon`: Summons the described card(s) on the field.
            - You can use for instance `Summon: Moss Dragon\, 15\, Trap 2` to summon a MD at level 15 with trap 2 as an additional skill.
            - You can also use `Summon: Moss Dragon\, 15 & Thunder Dragon\, 13` to summon a level 15 MD and a level 13 TD
            - The backslashes before commas are required in the summon's definition
            - You can chain as many cards as you want in the summon with `&` but one summon skill won't work if any of it's cards are already on the field.
            - You can use `QS_` or `D_` if you want this skill to summon only when entering the field or when being killed
        - Issue #141: `Am Retractation`: When entering the field, exiles all other cards from the field and the hand of both players. Demons aren't affected.
        - Issue #143: `Protection`: Reduces attack damage by X % (same as jungle barrier, marsh barrier... except without an attacking card type needed)
        - Issue #143: `Full Protection`: Reduces ALL kind of damages by X %
        - Issue #144: `Triple Mana Corruption`: Like mana corruption but targets 3 random cards.
        - Issue #145: `Explosion`: Does X + ((X-30) / 2) * number of cards in opposite field fire damage. (same as frost shock except no status effect)
    - Sim now allows wait times over 10 when using full description
    - Issue #149: New bundled file in each release: `evolve_skills.list.txt` contains all the evolves by rank.
    - New bundled file in each release: `skills.list.txt` contains all the skills descriptions.
    
- **Bugfixes**
    - Debug mode now works properly.
    - Fixed Sacrifice not taking buffs into account.
    - Useless skills of Special cards are now ignored by the sim.
    - Fixed some characters not being replaced in RM deck extraction file names.
    - If level is set above 15 the sim now outputs a user-friendly error

- **Tech. Changes**
    - Sim size divided by 2. You'll download it faster !

0.6.7 - 2015-09-11
--------------------

** Note: Your KW results will change drastically due to a bug fix in this version. They are now much more accurate.**

- **Features**
    - DI Simulation now prints the average merit per gem (MPG).
    - Hydra Simulation: You can now control the threshold at which you want to switch deck using the `--deck-switch-at-hp #` or `-dsh #` option. This has 2 impacts:
        - When using the `-dka` option, the sim's deck switching will occur once the hydra's deck HP goes under `#` instead of when it's isolated.
        - Instead of outputing the table of battles to isolate Hydra, the sim will output the battles to get hydra under `#` HP.
    - Issue #137: Now prints average number of hits to kill hydra or to reach the deck switch condition as first column of the outputs
    - Issue #135: New LoA skill Critical Counterattack.
    - Issue #140: New parameterizable card tokens: `ATK#multiplier:` and `HP#multiplier:` to boost attack and hp by the provided multiplier
    
- **Bugfixes**
    - Issue #139: Fix cli options going haywire
    - Fixed a bug in KW simulations that made results much higher than what they actually were.

0.6.6a - 2015-09-07
--------------------
    
- **Bugfixes**
    - Fixed hydra and KW CLI options not behaving as intended

0.6.6 - 2015-09-06
--------------------

- **Features**
    - New local database updater tool allows you to load a local version of the database to override the bundled database.
      In case of new cards being released this would make them available to you before the next sim release.
    - New file added to release: `kw_store.list.txt`
    - Supported cards lists is now divided into per-star sections
    - Issue #132: Added LoA rune Dragon Howling
    
- **Bugfixes**
    - Issue #130: Fixed an error when running Versus sim without the `-i` option.
    - Fixed a bug with weaken-like abilities where ATK would go negative

- **Tech. Changes**
    - Adapted default iterations for versus mode to scale between 50k and 5k depending on the quantity of defender decks there is.
    - Versus sim now outputs number of iterations and time spent as other sims do. 

0.6.5 - 2015-08-24
--------------------

**Attention ! You will need an updated version of Java from now on. Make sure you [download Java 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html)**

- **Features**
    - New card token: `LG:` Marks a card as being legendary. This token should be used on all cards of an EW defending deck to automatically apply the attack and health multipliers.
    
- **Bugfixes**
    - Fixed --skip-shuffle order beeing inverted

- **Tech. Changes**
    - Updated card DB: Aegis Dragon, Cancer, Horde General, Plasma Dragon, Skywalker
    - Switched to using Java 8 instead of Java 7

0.6.4 - 2015-08-17
--------------------

- **Features**
    - Issue #86: **Level 1 2 and 3 runes are now supported. For each rune, you can provide a level (`Permafrost:L0`) but also any value you'd like (`Permafrost:500`). If nothing is specified, level 4 is assumed.**
    - Abilities can now be referenced with their in game name with level. For instance you can use `Craze 7` instead of `Craze:70` or `Quick Strike: Blizzard 8` instead of `QS_Blizzard:160`
    - New abilities: Barricade, Group Morale

- **Bugfixes**
    - Issue #126: Fixed Arcane Magic specific runes
    - Fixed rune levels of KW NPCs 

0.6.3 - 2015-08-12
--------------------

- **Features**
    - Issue #36, #115**Introducing card tokens: placed before a card name, these will allow cards to endorse a specific role in the deck.**
        - `E:` Marks a card as being an event (Hydra, EW) card and thus boosts it's attack and health depending on it's number of stars (e.g. `E:The Butcher` would create The Butcher at level 10 with 3x attack and 3x health).
        - `DI:` Marks a card as being a DI Merit card. Each card with this token will add 8k merit to each fight.
    - Issue #60: **Hydra Sim improvement: you can now provide an alt deck with the `-dka other_deck.txt` option so that the sim automatically switches deck when hydra is isolated.** 
    - Improved some outputs

0.6.2 - 2015-08-10
--------------------

- **Bugfixes**
    - Fixed how purification works

0.6.1a - 2015-08-08
--------------------

- **Bugfixes**
    - Fixed attack not beeing correctly calculated with `bloodthirsty`, `weaken` and other attack-changing skills

0.6.1 - 2015-08-07
--------------------

** WARNING: due to the fixed bug on the craze glitch, your DI results for the decks using it may change drastically if you were using more than one Force card ! **

- **Features**
    - Print base cost for DI Sim

- **Bugfixes**
    - Fixed craze-glitch implementation.

- **Tech. Changes**
    - Updated card DB: Aries, Blackfire Assassin, Malevolent Dryad, Phantom Guardian, The Butcher

0.6.0 - 2015-07-24
--------------------

- **Features**
    - **Issue #28: New simpler card definition: `Card Name, level[+evo], New Ability 1, ..., New Ability N`**
    - Issue #72: DI card cost auto calculated when using the new card definition
    - DI Sim can now be used with custom demons by passing a full card's description instead of just the demon's name.

0.5.1 - 2015-07-23
--------------------

- **Bugfixes**
    - Updated Pazuzu

0.5.0 - 2015-07-16
--------------------

- **Features**
    - **Added Elemental War mode ! Check the wiki for more explanations.**

0.4.9 - 2015-07-11
--------------------

- **Features**
    - Updated card entries

- **Bugfixes**
    - Fixed some rune's activation conditions: Blight Stone, Burning Soul, Clear Spring, Frost, Lightning, Permafrost, Red Lotus

0.4.8 - 2015-07-07
--------------------

- **Features**
    - New runes: Blight Stone, Divine Plea

0.4.7 - 2015-07-05
--------------------

- **Bugfixes**
    - Doesn't crash if a server is down but notifies the user gently.
    - Fixed Dread Roar and Terror Roar

0.4.6 - 2015-07-01
--------------------

- **Features**
    - New abilities: Dread Roar, Terror Roar, Death Marker

- **Bugfixes**
    - Fixed Last Chance now allowing Laceration
    - Fixed Last Chance taking over defensive runes (Arctic freeze, Stonewall...)

0.4.5 - 2015-06-28
--------------------

- **Features**
    - New abilities: Bloody Battle, Last Chance

- **Tech. Changes**
    - Renammed Perseverance to Am Perseverance
    - Skill stubs waiting to be implemented: Terror Roar, Death Marker

0.4.4 - 2015-06-25
--------------------

- **Features**
    - **Added an extracting tool for FoH decks available through `--extract-foh`** 

- **Bugfixes**
    - Issue #122: Fixed summons beeing kept from one battle to the other in the KW sim
    - Fixed arena extraction for decks using The Don
    - Fixed duration of silence
    - Fixed Summon Dragon to summon a Thunder Dragon
    - Fixed how cards are put back in the deck (reincarnation, exile, no more space in hand)
    
- **Tech. Changes**
    - **Extract arena output directory option has been renamed from `-ermo` to `-out` to be used for foh deck extraction too**

0.4.3a - 2015-06-15
--------------------

- **Features**
    - **New DI Sim output now prints a bar chart for rounds and MPF repartition !**
    - **New KW Sim output now prints a bar chart for consecutive fights repartition !**

- **Bugfixes**
    - Issue #108: Empty folders don't put the sim in an eternal waiting state anymore.
    - Fixed silence
    - Fixed purification regarding silence
    - Fixed The Don
    - Fixed deck extraction for decks containing summonned cards
    
- **Tech. Changes**
    - Made hydra output more clear concerning the second table which is `Battles to isolate hydra`
    - Shows which file is in error if an error occurs in vs mode.

0.4.2 - 2015-06-12
--------------------

- **Features**
    - New abilities: Second Wind, Gang Up!, The Don's Bodyguard, Summon Dragon
    - Added fd_54 and fd_55 as KW decks
    - Added demon PazuzuCN (chinese EK version of Pazuzu) for testing
    - Added cards: The Don, Dragon Summoner, Ancient Garuda, Angelia the Pure

- **Bugfixes**
    - Remove overrided type (by corruption-like abilities) on death
    - Fixed a bug when extracting decks using cards under level 10
    - Fixed purification

0.4.1 - 2015-06-04
--------------------

- **Features**
    - New abilities: Corruption, Holy Light, Lava Trial, Naturalize

- **Bugfixes**
    - Issue #106: fixed HP boosts beeing kept from one battle to an other in KW sim
    - Fixed silence for cards with Guard
    
- **Tech. Changes**
    - Changed the name of KW guard id_52_2 and id_52_1 to id_52_rl3 and id_52_rl4 so that people know which is what (rl stands for rune level)

0.4.0 - 2015-06-03
--------------------

- **Features**
    - **Added Kingdom War mode which allows to simulate multiple consecutive kw battles against all or some of the guards**

- **Bugfixes**
    - Fixed FoH outputing json text
    
- **Tech. Changes**
    - Renamed Purify to Purification as per EK's skill list
    - Made shield visible in supported abilities as it's referenced in EK's skill list
    - Improved some outputs encoding

0.3.7 - 2015-05-29
--------------------

- **Bugfixes**
    - Tweeked `silence` to behave more like it does in game
    - Issue #100: Resurrection is now REALLY nullified

0.3.6 - 2015-05-27
--------------------

- **Features**
    - **Added an extracting tool for RM decks available through `--extract-rm`** 

- **Bugfixes**
    - Tweeked `silence` to behave more like it does in game
    - Issue #99: Resurrection is now nullified

0.3.5 - 2015-05-25
--------------------

**Note: Silence will affect ALL cards powers except resistance and aura-types (forest force, moutain guard...) for now. I'll be waiting for concrete information before changng this.**

- **Bugfixes**
    - Issue #94: Fixed `silence` crashing the sim.

0.3.4 - 2015-05-25
--------------------

** Breaking change: you now need to provide the deck file if you want to run a sim that uses one. **

- **Features**
    - New skill: `silence`
    - Added upcoming demons: Pazuzu, Azathoth, Bahamut
    - Issue #45: `-vs` options now allows folder paths to sim against all the files in it.
    
- **Tech. Changes**
    - Removed deck option defaulting to deck.txt

0.3.3 - 2015-05-16
--------------------

- **Features**
    - Issue #85: Added `Frost Shock` and `Shield of Earth` abilities.

- **Bugfixes**
    - Issue #79: Status ailments now get removed at the beginning of the turn but still apply to the whole turn.
    - Issue #80: Fixed starting player when it's a tie 
    - Fixed a crash when odds are missing from the EK Data in FoH.

0.3.2 - 2015-04-22
--------------------

** WARNING: due to the fixed bug on Hot Chase, your PO DI results may change drastically ! **

- **Features**
    - Issue #58: Added `apollo` (BlackBerry EK) server to FoH auto-loaded servers.
    - Issue #64: Added `skorn` (Russian EK) server to FoH auto-loaded servers.
    - Issue #65: FoH results now also show current odds
    - Added cards to-be-released: Arctic Overlord, Chimera, Gilgamesh, Hephaestus, Voodoo Scarecrow, Suchos

- **Bugfixes**
    - Issue #68: Fixed Divine Protection beeing applied twice on originating card
    - Issue #70: Fixed Hot Chase and Vendetta values not being multiplied
    - Fixed a rare bug in Counterattack
    - Fixed crash of FoH sim between 23h and 00h server time
    
- **Tech. Changes**
    - Fixed FoH rounding output

0.3.1 - 2015-04-16
--------------------

- **Features**
    - Issue #48: If a deck error occurs it now shows the deck file name
    - Issue #56: Added `-demon-show-damage` option
    - Issue #59: Added one digit to FoH percents

- **Bugfixes**
    - Issue #55: Fixed retaliation crashing in rare conditions
    - Fix crash on incomplete EK FoH data

0.3.0 - 2015-04-13
--------------------
    
- **Features**
    - **Added FoH mode which allows to autoload and sim all FoH battles**
    - Issue #54: Validates that no duplicate runes are in the deck
    - Issue #51: Added bunnies and [Elite] cards to supported.cards.txt

0.2.8 - 2015-04-11
--------------------

- **Features**
    - Hydra now prints % of battles where only the hydra remains

0.2.7 - 2015-04-08
--------------------

- **Features**
    - Added rune "ghost step 2mtn" for magic realm. 
    - Added "Silver Swordmaster" and "Bloody Host" to supported cards
    
- **Bugfixes**
    - Figured out the formula that defines which player starts. Versus battles aren't random anymore.

0.2.6 - 2015-04-06
--------------------
    
- **Bugfixes**
    - Fixed --threads cli option
    - Issue #49: Ghost Step has been changed to activate when 3 mountain cards are in the field.
    - Issue #50: fixed Dual Snipe

0.2.5 - 2015-03-31
--------------------
    
- **Features**
    - Issue #26: Added hydra-only sub-mode using cli option --hydra-only
    - Issue #38: Added Perseverance skill for Arcane Magic
    - Issue #39: Added AmReanimation skill for Arcane Magic
    - Added new card: Inferno Treant
    
- **Bugfixes**
    - Issue #41: Fixed obstinacy healing instead of damaging
    
- **Tech. Changes**
    - Improved single and multi-opponent outputs

0.2.4 - 2015-03-29
--------------------

- **Bugfixes**
    - Fixed rune inferno targetting self
    - Fixed desperations when more than one

0.2.3 - 2015-03-27
--------------------

** WARNING: due to the fixed bug thieves and hydras, your results for those two modes may change drastically depending on your deck **
    
- **Features**
    - Issue #31: Added new deck option --always-start to force a deck to be the first to draw a card
    - Issue #32: Added sacred shield power for arcane magic
    
- **Bugfixes**
    - Issue #34: Fix player HP over level 100
    - Fixed Thieves and Hydras: now the NPC is always the first to draw a card


0.2.2 - 2015-03-26
--------------------
    
- **Features**
    - Issue #22: A warning will now be shown if deck cost > player cost
    
- **Bugfixes**
    - Issue #19: Fixed Reflection + Resurrection combo

- **Tech. Changes**
    - Cleaned a bit the debug output
    - Result output was rearranged a bit


0.2.1 - 2015-03-24
--------------------

** WARNING: due to the fixed bug on chain attack, your SK DI results may change drastically if you used more than 2 of the same cards in your deck **

- **Features**
    - Issue #20: Added new deck option --skip-shuffle in deck file to force the card order
    - Issue #25: Added Arcane Magic demons
    - Issue #27: Added Arcane Magic specific runes: "am bouclier de lave", "am envol", "am feu de forge" and "am tsunami"

- **Bugfixes**
    - Issue #23: Fixed Coldwave and Avalanche not applied correctly
    - Issue #23: Fixed clean sweep and chain attack only targetting one other card
    - Fixed some runes activating even though they had no target


0.2.0 - 2015-03-23
--------------------

- **Features**
    - **Added Hydra mode which allows to simulate hydra battles against all or some hydras**
    - Added Purify ability
    - Added 2 subtypes of legendary thieves

- **Tech. Changes**
    - All merit and round metrics are now calculated more precisely


0.1.2 - 2015-03-16
--------------------

- **Bugfixes**
    - Issue #12: Now reports a clear error if type is misspelled 
    - Issue #14: Fixed QS_exile on a destroyed card (card is not exiled and dies)
    - Issue #15: Fixed retaliation on a card destroyed by a chain destroy started by the retaliation itself

- **Features**
    - Shows chances for consecutive hits on a thief


0.1.1 - 2015-03-14
--------------------

- **Bugfixes**
    - Fixed mising "-" in card names
    - Fixed Barrier abilities
    - Fixed Dual Snipe
    - Fixed Trap:1 when no card in enemy deck
    - Fixed prayer on infinite HP bosses
    - Fixed retaliation and self destruct damaging order
    - Fixed order of counter vs desperation when a card is killed by an attack

- **Features**
    - Now reports an error if deck doesn't have at least one card

0.1.0 - 2015-03-11
--------------------

- **Bugfixes**
    - Fixed line endings of released files (always CRLF)
    - Fixed invalid damage value used for bloodsucker
    - Fixed trap
    - Fixed fire god

- **Features**
    - **Added Versus mode which allows to simulate deck vs deck battles**
    - **Added Thief mode which allows to simulate battles against thieves**
    - Added an option (--no-progress-bar or -npb) to hide the progress bar
    - God of Salvation renamed  Astral Sentinel
    - New abilities:
        - arctic pollution
        - bite
        - blight
        - blitz
        - blizzard
        - chain lightning
        - clean sweep
        - combustion
        - confusion
        - electric shock
        - feast of blood
        - fire wall
        - fireball
        - firestorm
        - forest fire
        - glacial barrier
        - group weaken
        - iceball
        - jungle barrier
        - magic shield
        - marsh barrier
        - mountain glacier
        - nova frost
        - plague
        - puncture
        - self-destruct
        - slayer
        - smog
        - swamp purity
        - teleportation
        - thunderbolt
        - venom
        - volcano barrier
        - weaken   
    - New runes:
        - arsenopyrite
        - avalanche
        - blizzard
        - burning soul
        - charred
        - coldwave
        - death zone
        - eruption
        - explosion
        - fire fist
        - fire flow
        - frost
        - heat wave
        - holy well
        - ice wall
        - icicle
        - inferno
        - lightning
        - methane
        - mineral
        - permafrost
        - quicksand
        - raised flag
        - red lotus
        - shock
        - stone forest
        - swampwater
        - thunderbolt
        - thunderstorm
        - tornado
        - transparency
        - wasteland

0.0.4 - 2015-03-03
--------------------

- **Bugfixes**
    - Fixed Trap

- **Features**
    - Updated card list
    - Can now provide player name in deck file for reference
    - New abilities:
        - Seal
    - New runes:
        - Ghost step


0.0.3 - 2015-03-01
--------------------

- **Breaking changes**
    - Player level must now be provided in deck file using "player level: 90"

- **Features**
    - Now prints the version on startup
    - Validates number of cards and number of runes on startup
    - User errors should now be more understandable
    - CHANGELOG (this file) is now bundled into the release

- **Bugfixes**
    - Added back the old demons


0.0.2 - 2015-02-27
--------------------

- **Breaking changes**
    - Ability Reincarnate renamed Reincarnation
    - Ability Lacerate renamed Laceration

- **Bugfixes**
    - Healing now doesn't crash if there's no card to heal

- **Features**
    - Card list updated with all normal / demon / hydra cards
    - New supported abilities
        - dual snipe
        - healing mist
        - impede
        - mass attrition
        - warcry

- **Tech. Changes**
    - Cards now have all abilities described, even unsupported ones to prepare future deck vs deck support.


0.0.1 - 2015-02-25
--------------------

**Initial version**