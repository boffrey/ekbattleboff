# Supported abilities
#
# This is the list of skills supported by the sim. Most skills require either a value or a level.
# You can reference a skill either by their full in game name (e.g. Prayer 7)
# or you can reference them wit ha custom value (e.g. Prayer:280)
#
# To mark a skill as being a quick strike or a depseration you can prefix them with the following modifiers:
# QuickStrike prefix: QS_ 
# Desperation prefix: D_
# For instance you can use QS_Prayer 7 or D_Prayer:280
#
# For more information on deck format, please check out the wiki page at
# https://bitbucket.org/Crystark/ek-battlesim/wiki/Advanced%20Deck%20building

Acrobatics
Advanced Rejuvenation
Advanced Strike
AM Healing
AM Perseverance
AM Reanimation
AM Regeneration
Apocalypse
Arctic Guard
Arctic Pollution
Asura's Flame
Backstab
Barricade
Bite
Bless
Blight
Blind
Blitz
Blizzard
Block
Bloodsucker
Bloodthirsty
Bloody Battle
Break Ice
Brutal Claw
Caesar's Strike
Cerberus
Chain Attack
Chain Lightning
Charging
City Defense
Clean Sweep
Cleanse
Cold Blood
Combustion
Concentration
Confusion
Corruption
Counterattack
Craze
Crazy
Curse
Damnation
Death Marker
Demon Skin
Destroy
Devil's Armor
Devil's Blade
Devil's Curse
Devour
Dexterity
Diana's Protection
Diana's Touch
Divine Protection
Divine Shield
Dodge
Double Attack
Double Wings
Drain Life
Dread Roar
Dual Snipe
Dying Strike
Electric Shock
Eruption
Evasion
Execution
Exile
Feast of Blood
Field of Chaos
Fire God
Fire Wall
Fireball
Firestorm
Flash
Forest Fire
Forest Force
Forest Guard
Frenzy
Frost Shock
Gang Up!
Glacial Barrier
Group Morale
Group Weaken
Guard
Healing
Healing Mist
Holy Light
Hot Chase
Ice Shield
Iceball
Immunity
Impede
Imperius
Infiltrator
Inspire
Jungle
Jungle Barrier
Laceration
Land of Lava
Last Chance
Lava Trial
Life Link
Machinegun
Magic Arrays
Magic Shield
Mana Burn
Mana Corruption
Mania
Marsh Barrier
Mass Attrition
Melody
Mountain Force
Mountain Glacier
Mountain Guard
Naturalize
Northern Force
Nova Frost
Obstinacy
Original Blessing
Origins Guard
Parry
Plague
Plague Blast
Plague Maid
Power Source
Prayer
Psychic Master
Psychic Master Hand
Puncture
Purification
Reanimation
Reflection
Regeneration
Reincarnation
Rejuvenation
Resistance
Resurrection
Retaliation
Road of Ashes
Sacred Flame
Sacrifice
Salvo
Seal
Second Wind
Self-Destruct
Sensitive
Shield
Shield of Earth
Silence
Slayer
Smog
Snipe
Soul Devour
Soul Imprison
Spell Mark
Spell Reduction
spine
Spiritual Voice
Storm Force
Summon
Summon Dragon
Summon Dragon II
Suppressing
Swamp Force
Swamp Guard
Swamp Purity
Teleportation
Terror Roar
The Don's Bodyguard
Thunderbolt
Time Reverse
Touch of Vampire
Toxic Clouds
Trap
Vendetta
Venom
Violent Storm
Volcano Barrier
Warcry
Warpath
Water Shield
Weaken
Wicked Leech
