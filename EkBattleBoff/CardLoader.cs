﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace EkBattleBoff
{
    internal class CardLoader
    {
        private static JArray cardDetails; 

        public CardLoader()
        {
        }

        internal List<Card> Load()
        {
            var cards = new List<Card>();
            cards.Add(new Card { Name = "<none>" });

            string line;
            var file = new StreamReader(@".\ek-battlesim\cards.supported.txt");
            while ((line = file.ReadLine()) != null)
            {
                line = line.Trim();
                if (line.StartsWith("#")) continue;
                if (line.Length == 0) continue;

                var props = line.Split(',').Select(s => s.Trim()).ToList();

                var card = new Card
                {
                    Name = props[0],
                    Cost = int.Parse(props[1]),
                    Atk = int.Parse(props[2]),
                    Hp = int.Parse(props[3]),
                    Kingdom = props[4],
                    Abilities = props.Skip(5).ToList()
                };
                cards.Add(card);
            }
            file.Close();


            return cards;
        }

        internal List<Card> LoadRunes()
        {
            var runes = new List<Card>();
            runes.Add(new Card { Name = "<no rune>" });

            string line;
            var file = new StreamReader(@".\ek-battlesim\runes.supported.txt");
            while ((line = file.ReadLine()) != null)
            {
                line = line.Trim();
                if (line.StartsWith("#")) continue;
                if (line.Length == 0) continue;

                var props = line.Split(',').Select(s => s.Trim()).ToList();

                var rune = new Card
                {
                    Name = props[0]
                };
                runes.Add(rune);
            }
            file.Close();


            return runes;
        }

        public string GetImageUrl(CardInstance selectedCard)
        {
            var cardId = FindCardId(selectedCard);

            return @"http://cache.ifreecdn.com/sgzj/public/swf/card/370_570/img_maxCard_" + cardId + ".jpg";
        }

        private JObject FindCard(CardInstance selectedCard)
        {
            if (cardDetails == null)
            {
                LoadCardDetails();
            }

            foreach (dynamic card in cardDetails)
            {
                if (card.CardName != selectedCard.Card.Name) continue;

                return (JObject)card;
            }

            return null;
        }
        private int FindCardId(CardInstance selectedCard)
        {
            dynamic card = FindCard(selectedCard);
            if (card == null) return -1;
            return card.CardId;
        }

        private void LoadCardDetails()
        {
            using (var client = new WebClient())
            {
                var cards = client.DownloadString("http://ekcardlist.azurewebsites.net/api/card?order=asc&limit=10&offset=0");
                cardDetails = JArray.Parse(cards);
            }
        }

        public int GetAtk(CardInstance selectedCard)
        {
            dynamic card = FindCard(selectedCard);
            return card.Attack;
        }
    }
}