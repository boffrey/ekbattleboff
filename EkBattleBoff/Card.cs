﻿using System.Collections.Generic;

namespace EkBattleBoff
{
    public class Card
    {
        public List<string> Abilities { get; internal set; }
        public int Atk { get; internal set; }
        public int Cost { get; internal set; }
        public int Hp { get; internal set; }
        public string Kingdom { get; internal set; }
        public string Name { get; set; }
        
        public override string ToString()
        {
            return Name;
        }
    }
}