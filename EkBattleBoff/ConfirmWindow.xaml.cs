﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace EkBattleBoff
{
    /// <summary>
    /// Interaction logic for MessageWindow.xaml
    /// </summary>
    public partial class ConfirmWindow : Window
    {
        public bool Result { get; private set; }

        public ConfirmWindow()
        {
            InitializeComponent();
        }

        private void YesButton_Click(object sender, RoutedEventArgs e)
        {
            Result = true;
            Close();
        }

        private void NoButton_Click(object sender, RoutedEventArgs e)
        {
            Result = false;
            Close();
        }

        public static bool Confirm(string message)
        {
            var dl = new ConfirmWindow();
            dl.Owner = Application.Current.MainWindow;
            dl.Message.Text = message;
            dl.Result = false;
            dl.ShowDialog();

            return dl.Result;
        }
    }
}
