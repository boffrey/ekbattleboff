﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EkBattleBoff
{
    internal class DeckWriter
    {
        public static string LastLoadedDeck { get; private set; }

        public DeckWriter()
        {
        }

        public void Write(string deckName, List<CardInstance> deckItems, int playerLevel=130, string path = "decks", bool overwrite = false)
        {
            Directory.CreateDirectory(path);

            var deckWithPath = path + "\\" + deckName;

            if (!overwrite && deckName!= LastLoadedDeck && File.Exists(deckWithPath))
            {
                if(!ConfirmWindow.Confirm("This deck file name already exists - Overwrite existing deck?"))
                {
                    return;
                }
            }

            using (var file = File.CreateText(deckWithPath))
            {
                file.WriteLine("# Deck file created by EkBattleBoff");
                file.WriteLine("# https://www.bitbucket.org/boffrey/ekbattleboff");
                file.WriteLine();
                file.WriteLine("Player level: " + playerLevel);
                file.WriteLine();

                foreach (var card in deckItems.Where(d => d.Card.Name!="<none>" && d.Card.Name!="<no rune>"))
                {
                    if(card.IsRune)
                    {
                        WriteRune(file, card);
                    }
                    else
                    {
                        WriteCard(file, card);
                    }
                }
            }
        }

        private void WriteCard(StreamWriter file, CardInstance card)
        {
            file.Write(card.Card.Name);

            file.Write(", ");
            file.Write(card.Level);
            if (!string.IsNullOrEmpty(card.EvoSkill))
            {
                file.Write(", ");
                file.Write(card.EvoSkill);
            }
            file.WriteLine();
        }

        private void WriteRune(StreamWriter file, CardInstance card)
        {
            file.Write(card.Card.Name);

            file.Write(":L");
            file.Write(card.Level);
            
            file.WriteLine();
        }

        public List<string> GetDeckNames()
        {
            var result = new List<string>();

            Directory.CreateDirectory("decks");
            var deckFiles = Directory.EnumerateFiles("decks").Select(d => d.Substring(d.IndexOf(@"\")+1));

            result.AddRange(deckFiles);

            return result;
        }

        internal List<CardInstance> LoadDeck(string deckName)
        {
            Directory.CreateDirectory("decks");

            var deckWithPath = "decks\\" + deckName;

            if (!File.Exists(deckWithPath))
            {
                MessageWindow.ShowMessage("This deck file name does not exist!");
                return null;
            }

            var result = new List<CardInstance>();

            using (var file = File.OpenText(deckWithPath))
            {
                var line = "";
                while((line = file.ReadLine()) != null)
                {
                    line = line.Trim();
                    if (string.IsNullOrEmpty(line)) continue;
                    if (line.StartsWith("#")) continue;
                    if (line.ToLower().StartsWith("player level")) continue;

                    var args = line.Split(',').Select(a => a.Trim()).ToArray();

                    if (args.Length==1 && args[0].Contains(":"))
                    {
                        // parse rune
                        args = args[0].Split(':').Select(a => a.Trim()).ToArray();

                        if(args.Length==2 && args[1].StartsWith("L") && args[1].Length==2)
                        {
                            args[1] = args[1].Substring(1);
                        }
                    }

                    var card = new CardInstance
                    {
                        Card = new Card
                        {
                            Name = args[0]
                        },
                        Level = int.Parse(args[1])
                    };
                    
                    if (args.Length > 2) card.EvoSkill = args[2];

                    result.Add(card);
                }
            }

            LastLoadedDeck = deckName;
            return result;
        }

        public void Delete(string deckName)
        {
            File.Delete(@"decks\" + deckName);
        }
    }
}