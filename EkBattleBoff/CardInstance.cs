﻿namespace EkBattleBoff
{
    public class CardInstance
    {
        public Card Card { get; set; }
        public int Level { get; set; }
        public string EvoSkill { get; set; }
        public bool IsRune { get; set; }
    }
}