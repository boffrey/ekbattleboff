﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Threading;

namespace EkBattleBoff
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private List<CardInstance> deckItems;
        private List<Card> cards;
        private List<Card> runes;
        private List<Card> filteredCards;

        public MainWindow()
        {
            InitializeComponent();
            
            cards = new CardLoader().Load();
            runes = new CardLoader().LoadRunes();
            filteredCards = cards.Select(c => new Card { Name = c.Name }).ToList();
            CardList.ItemsSource = filteredCards;

            // get all demons
            var demons = cards.Where(c => c.Abilities != null && c.Abilities.Count > 0 && c.Abilities[0] == "demon").ToList();
            DemonSelector.ItemsSource = demons;

            // get all EW cards
            var ewItems = cards.Where(c => c.Abilities != null && c.Abilities.Count > 0 && c.Abilities[0] == "hydra" && c.Name.EndsWith(" (Legendary)")).Select(c => c.Name.Replace(" (Legendary)", "")).ToList();
            EwSelector.ItemsSource = ewItems;

            InitializeDeck();

            CardList.AddHandler(UIElement.MouseDownEvent,
                new MouseButtonEventHandler(CardList_MouseLeftButtonDown), true);
            
            FilterTextBox.Text = FilterTextBox.Tag as string;
            FilterTextBox.GotFocus += RemoveText;
            FilterTextBox.LostFocus += AddText;

            DeckFileName.Text = DeckFileName.Tag as string;
            DeckFileName.GotFocus += RemoveText;
            DeckFileName.LostFocus += AddText;

            DeckFileList.ItemsSource = new DeckWriter().GetDeckNames();
            Title = "EKBattleBoff - Using " + new Simulator().GetVersion();
        }

        private void CheckForNewVersion()
        {
            var sim = new Simulator();
            var version = sim.CheckForUpdates();
            if (version != null &&
               ConfirmWindow.Confirm("A new version of Crystark's ek-battlesim is available\r\n\r\nWould you like to download it now?"))
            {
                sim.DownloadNewVersion(version);
                MessageWindow.ShowMessage("New version of Crystark's ek-battlesim is now installed.");
                Title = "EKBattleBoff - Using " + new Simulator().GetVersion();
            }
        }

        private void InitializeDeck()
        {
            // fill the default deck items
            deckItems = new List<CardInstance>();
            for (var i = 0; i < 10; i++)
            {
                deckItems.Add(new CardInstance { Card = cards.First(), Level = 10 });
            }
            for (var i = 0; i < 4; i++)
            {
                deckItems.Add(new CardInstance { Card = runes.First(), Level = 4, IsRune = true });
            }
            DeckList.ItemsSource = deckItems;
            DIDeckList.ItemsSource = deckItems;
            EWDeckList.ItemsSource = deckItems;
        }

        #region ScaleValue Dependency Property
        public static readonly DependencyProperty ScaleValueProperty = DependencyProperty.Register("ScaleValue", typeof(double), typeof(MainWindow), new UIPropertyMetadata(1.0, new PropertyChangedCallback(OnScaleValueChanged), new CoerceValueCallback(OnCoerceScaleValue)));

        private static object OnCoerceScaleValue(DependencyObject o, object value)
        {
            MainWindow mainWindow = o as MainWindow;
            if (mainWindow != null)
                return mainWindow.OnCoerceScaleValue((double)value);
            else
                return value;
        }

        private static void OnScaleValueChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            MainWindow mainWindow = o as MainWindow;
            if (mainWindow != null)
                mainWindow.OnScaleValueChanged((double)e.OldValue, (double)e.NewValue);
        }

        protected virtual double OnCoerceScaleValue(double value)
        {
            if (double.IsNaN(value))
                return 1.0f;

            value = Math.Max(0.1, value);
            return value;
        }

        protected virtual void OnScaleValueChanged(double oldValue, double newValue)
        {

        }

        public double ScaleValue
        {
            get
            {
                return (double)GetValue(ScaleValueProperty);
            }
            set
            {
                SetValue(ScaleValueProperty, value);
            }
        }
        #endregion


        #region placeholder for filter box

        public void RemoveText(object sender, EventArgs e)
        {
            var box = (TextBox)sender;
            if(box.Text==box.Tag) box.Text = "";
        }

        public void AddText(object sender, EventArgs e)
        {
            var box = (TextBox)sender;
            
            if (String.IsNullOrWhiteSpace(box.Text))
                box.Text = box.Tag as string;
        }
        #endregion

        private void MainGrid_SizeChanged(object sender, EventArgs e)
        {
            CalculateScale();
        }

        private void CalculateScale()
        {
            double yScale = ActualHeight / 600f;
            double xScale = ActualWidth / 500f;
            double value = Math.Min(xScale, yScale);
            ScaleValue = (double)OnCoerceScaleValue(myMainWindow, value);
        }

        private void listView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // update card detail in details tabs
            var selectedCard = DeckList.SelectedItem as CardInstance;
            if(selectedCard==null)
            {
                CardNameLabel.Content = string.Empty;
                return;
            }

            CardNameLabel.Content = selectedCard.Card.Name;
            CardLevel.Text = selectedCard.Level.ToString();

            if (selectedCard.Card.Abilities != null)
            {
                Ability1Label.Content = selectedCard.Card.Abilities[1];
                Ability2Label.Content = selectedCard.Card.Abilities[2];
                Ability3Label.Content = selectedCard.Card.Abilities[3];
            }
            else
            {
                Ability1Label.Content = string.Empty;
                Ability2Label.Content = string.Empty;
                Ability3Label.Content = string.Empty;
            }

            // update evo option text
            EvoText.Text = selectedCard.EvoSkill;

            // update full selector list to be either cards or runes
            if (selectedCard.IsRune)
            {
                CardList.ItemsSource = runes;
                CardList.Items.Refresh();
            }
            else
            {
                CardList.ItemsSource = cards;
                CardList.Items.Refresh();
            }

            UpdateCardImage(selectedCard);
        }

        private void UpdateCardImage(CardInstance selectedCard)
        {
            var cardLoader = new CardLoader();
            CardImage.DataContext = cardLoader.GetImageUrl(selectedCard);
            var cardImageDictionary = new Dictionary<string, Image>();
            cardImageDictionary.Add("forest", ForestCardBorder);
            cardImageDictionary.Add("mtn", MountainCardBorder);
            cardImageDictionary.Add("swamp", SwampCardBorder);
            cardImageDictionary.Add("tundra", TundraCardBorder);
            cardImageDictionary.Add("demon", DemonCardBorder);
            cardImageDictionary.Add("special", SpecialCardBorder);

            foreach (var key in cardImageDictionary.Keys)
            {
                var img = cardImageDictionary[key];
                if (selectedCard.Card.Abilities != null && selectedCard.Card.Abilities[0] == key) img.Visibility = Visibility.Visible;
                else img.Visibility = Visibility.Hidden;
            }
        }

        private void FilterTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (FilterTextBox.Text == (string)FilterTextBox.Tag) return;

            filteredCards.Clear();
            foreach(var item in cards)
            {
                var isVisibile = item.Name.ToLower().Contains(FilterTextBox.Text.ToLower());
                if (!isVisibile && item.Name!="<none>" && item.Name != "<no rune>") continue;

                filteredCards.Add(item);
            }

            CardList.ItemsSource = filteredCards;
            CardList.Items.Refresh();
        }

        private void CardList_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (CardList.SelectedItems.Count == 0) return;
            if (DeckList.SelectedIndex == -1) return;

            deckItems[DeckList.SelectedIndex].Card = ((Card)CardList.SelectedItems[0]);

            DeckList.Items.Refresh();
            DIDeckList.Items.Refresh();
            EWDeckList.Items.Refresh();
        }

        private void CardLevel_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (DeckList.SelectedIndex == -1) return;
            var card = deckItems[DeckList.SelectedIndex];

            int level;
            int.TryParse(CardLevel.Text, out level);

            card.Level = level;
            CardLevelSlider.Value = level;
            DIDeckList.Items.Refresh();
            EWDeckList.Items.Refresh();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (CardLevel == null) return;

            CardLevel.Text = ((int)CardLevelSlider.Value).ToString();
            DIDeckList.Items.Refresh();
            EWDeckList.Items.Refresh();
        }

        private void SaveButton_Click(object sender, RoutedEventArgs e)
        {
            if(DeckFileName.Text=="" || DeckFileName.Text==(string)DeckFileName.Tag)
            {
                MessageWindow.ShowMessage("Type a name for the deck to save it.");
            }

            var writer = new DeckWriter();
            writer.Write(DeckFileName.Text, deckItems);
            DeckFileList.ItemsSource = new DeckWriter().GetDeckNames();
            MessageWindow.ShowMessage("Deck has been saved.");
        }
        
        private void DeckFileList_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            // load deck file
            var deckFileName = (string)DeckFileList.SelectedItem;
            var cardList = new DeckWriter().LoadDeck(deckFileName);

            deckItems.Clear();
            deckItems.AddRange(cardList);

            // fix up cards so they have the full card detail associated
            foreach(var card in deckItems)
            {
                var actualCard = cards.FirstOrDefault(c => c.Name == card.Card.Name);

                if (actualCard == null)
                {
                    actualCard = runes.FirstOrDefault(r => r.Name == card.Card.Name);
                    if(actualCard==null) continue;
                    card.IsRune = true;
                }

                card.Card = actualCard;
            }

            // add blank card entries to deck
            while(deckItems.Count(c => !c.IsRune) < 10)
            {
                deckItems.Add(new CardInstance
                {
                    Card = cards.First(),
                    Level = 10
                });
            }

            // add blank rune entries to deck
            while (deckItems.Count(c => c.IsRune) < 4)
            {
                deckItems.Add(new CardInstance
                {
                    Card = runes.First(),
                    Level = 4,
                    IsRune = true
                });
            }

            DeckFileName.Text = (string)deckFileName;
            DeckList.Items.Refresh();
            DIDeckList.Items.Refresh();
            EWDeckList.Items.Refresh();
            MessageWindow.ShowMessage("Deck file loaded!");
        }

        private void EvoText_TextChanged(object sender, TextChangedEventArgs e)
        {
            var selectedCard = DeckList.SelectedItem as CardInstance;
            if (selectedCard == null) return;

            selectedCard.EvoSkill = EvoText.Text;
            DIDeckList.Items.Refresh();
            EWDeckList.Items.Refresh();
        }

        private void RunSimulator_Click(object sender, RoutedEventArgs e)
        {
            var sim = new Simulator();

            sim.RunDI(deckItems, DemonSelector.Text, int.Parse(PlayerLevel.Text), UpdateProgressBar);

            if (sim.ErrorMessage!=null)
            {
                MessageWindow.ShowMessage(sim.ErrorMessage);
                return;
            }

            Mpm.Content = sim.Mpm;
            Mpf.Content = sim.Mpf;
            Rounds.Content = sim.AvgRounds;
            DeckCooldown.Content = sim.DeckCooldown;

            var roundRepartitionWidgets = new List<RepartitionWidgets>
            {
                new RepartitionWidgets{ Label = RoundRepartition0, Bar = RoundRepartition0Bar, Percent = RoundRepartition0Percent },
                new RepartitionWidgets{ Label = RoundRepartition1, Bar = RoundRepartition1Bar, Percent = RoundRepartition1Percent },
                new RepartitionWidgets{ Label = RoundRepartition2, Bar = RoundRepartition2Bar, Percent = RoundRepartition2Percent },
                new RepartitionWidgets{ Label = RoundRepartition3, Bar = RoundRepartition3Bar, Percent = RoundRepartition3Percent },
                new RepartitionWidgets{ Label = RoundRepartition4, Bar = RoundRepartition4Bar, Percent = RoundRepartition4Percent },
                new RepartitionWidgets{ Label = RoundRepartition5, Bar = RoundRepartition5Bar, Percent = RoundRepartition5Percent },
                new RepartitionWidgets{ Label = RoundRepartition6, Bar = RoundRepartition6Bar, Percent = RoundRepartition6Percent },
                new RepartitionWidgets{ Label = RoundRepartition7, Bar = RoundRepartition7Bar, Percent = RoundRepartition7Percent },
                new RepartitionWidgets{ Label = RoundRepartition8, Bar = RoundRepartition8Bar, Percent = RoundRepartition8Percent },
                new RepartitionWidgets{ Label = RoundRepartition9, Bar = RoundRepartition9Bar, Percent = RoundRepartition9Percent }
            };

            UpdateRepartitionsWidgets(roundRepartitionWidgets, sim.RoundRepartitions);

            var mpfRepartitionWidgets = new List<RepartitionWidgets>
            {
                new RepartitionWidgets{ Label = MpfRepartition0, Bar = MpfRepartition0Bar, Percent = MpfRepartition0Percent },
                new RepartitionWidgets{ Label = MpfRepartition1, Bar = MpfRepartition1Bar, Percent = MpfRepartition1Percent },
                new RepartitionWidgets{ Label = MpfRepartition2, Bar = MpfRepartition2Bar, Percent = MpfRepartition2Percent },
                new RepartitionWidgets{ Label = MpfRepartition3, Bar = MpfRepartition3Bar, Percent = MpfRepartition3Percent },
                new RepartitionWidgets{ Label = MpfRepartition4, Bar = MpfRepartition4Bar, Percent = MpfRepartition4Percent },
                new RepartitionWidgets{ Label = MpfRepartition5, Bar = MpfRepartition5Bar, Percent = MpfRepartition5Percent },
                new RepartitionWidgets{ Label = MpfRepartition6, Bar = MpfRepartition6Bar, Percent = MpfRepartition6Percent },
                new RepartitionWidgets{ Label = MpfRepartition7, Bar = MpfRepartition7Bar, Percent = MpfRepartition7Percent },
                new RepartitionWidgets{ Label = MpfRepartition8, Bar = MpfRepartition8Bar, Percent = MpfRepartition8Percent },
                new RepartitionWidgets{ Label = MpfRepartition9, Bar = MpfRepartition9Bar, Percent = MpfRepartition9Percent }
            };

            UpdateRepartitionsWidgets(mpfRepartitionWidgets, sim.MpfRepartitions);
        }

        private void RunEwSimulator_Click(object sender, RoutedEventArgs e)
        {
            var sim = new Simulator();

            sim.RunEW(deckItems, EwSelector.Text, int.Parse(EwPlayerLevel.Text), UpdateEwProgressBar);

            if (sim.ErrorMessage != null)
            {
                MessageWindow.ShowMessage(sim.ErrorMessage);
                return;
            }

            Mpm.Content = sim.Mpm;
            Mpf.Content = sim.Mpf;
            Rounds.Content = sim.AvgRounds;
            DeckCooldown.Content = sim.DeckCooldown;

            var roundRepartitionWidgets = new List<RepartitionWidgets>
            {
                new RepartitionWidgets{ Label = RoundRepartition0, Bar = RoundRepartition0Bar, Percent = RoundRepartition0Percent },
                new RepartitionWidgets{ Label = RoundRepartition1, Bar = RoundRepartition1Bar, Percent = RoundRepartition1Percent },
                new RepartitionWidgets{ Label = RoundRepartition2, Bar = RoundRepartition2Bar, Percent = RoundRepartition2Percent },
                new RepartitionWidgets{ Label = RoundRepartition3, Bar = RoundRepartition3Bar, Percent = RoundRepartition3Percent },
                new RepartitionWidgets{ Label = RoundRepartition4, Bar = RoundRepartition4Bar, Percent = RoundRepartition4Percent },
                new RepartitionWidgets{ Label = RoundRepartition5, Bar = RoundRepartition5Bar, Percent = RoundRepartition5Percent },
                new RepartitionWidgets{ Label = RoundRepartition6, Bar = RoundRepartition6Bar, Percent = RoundRepartition6Percent },
                new RepartitionWidgets{ Label = RoundRepartition7, Bar = RoundRepartition7Bar, Percent = RoundRepartition7Percent },
                new RepartitionWidgets{ Label = RoundRepartition8, Bar = RoundRepartition8Bar, Percent = RoundRepartition8Percent },
                new RepartitionWidgets{ Label = RoundRepartition9, Bar = RoundRepartition9Bar, Percent = RoundRepartition9Percent }
            };

            UpdateRepartitionsWidgets(roundRepartitionWidgets, sim.RoundRepartitions);

            var mpfRepartitionWidgets = new List<RepartitionWidgets>
            {
                new RepartitionWidgets{ Label = MpfRepartition0, Bar = MpfRepartition0Bar, Percent = MpfRepartition0Percent },
                new RepartitionWidgets{ Label = MpfRepartition1, Bar = MpfRepartition1Bar, Percent = MpfRepartition1Percent },
                new RepartitionWidgets{ Label = MpfRepartition2, Bar = MpfRepartition2Bar, Percent = MpfRepartition2Percent },
                new RepartitionWidgets{ Label = MpfRepartition3, Bar = MpfRepartition3Bar, Percent = MpfRepartition3Percent },
                new RepartitionWidgets{ Label = MpfRepartition4, Bar = MpfRepartition4Bar, Percent = MpfRepartition4Percent },
                new RepartitionWidgets{ Label = MpfRepartition5, Bar = MpfRepartition5Bar, Percent = MpfRepartition5Percent },
                new RepartitionWidgets{ Label = MpfRepartition6, Bar = MpfRepartition6Bar, Percent = MpfRepartition6Percent },
                new RepartitionWidgets{ Label = MpfRepartition7, Bar = MpfRepartition7Bar, Percent = MpfRepartition7Percent },
                new RepartitionWidgets{ Label = MpfRepartition8, Bar = MpfRepartition8Bar, Percent = MpfRepartition8Percent },
                new RepartitionWidgets{ Label = MpfRepartition9, Bar = MpfRepartition9Bar, Percent = MpfRepartition9Percent }
            };

            UpdateRepartitionsWidgets(mpfRepartitionWidgets, sim.MpfRepartitions);
        }

        private void UpdateRepartitionsWidgets(List<RepartitionWidgets> repartitionWidgets, List<Simulator.Repartition> repartitions)
        {
            var idx = 0;
            foreach (var widgets in repartitionWidgets)
            {
                widgets.Label.Visibility = Visibility.Hidden;
                widgets.Bar.Visibility = Visibility.Hidden;
                widgets.Percent.Visibility = Visibility.Hidden;
                if (idx >= repartitions.Count) continue;

                widgets.Label.Content = repartitions[idx].Label;
                widgets.Bar.Width = (repartitions[idx].PercentageValue * 160d) / 100d;
                widgets.Percent.Content = repartitions[idx].Percentage;
                widgets.Label.Visibility = Visibility.Visible;
                widgets.Bar.Visibility = Visibility.Visible;
                widgets.Percent.Visibility = Visibility.Visible;

                idx++;
            }
        }

        private void UpdateProgressBar(string percent, int pctValue)
        {
            SimulatorProgress.Minimum = 0;
            SimulatorProgress.Maximum = 100;
            
            SimulatorProgress.Dispatcher.Invoke(() => SimulatorProgress.Value = pctValue, DispatcherPriority.Background);
        }

        private void UpdateEwProgressBar(string percent, int pctValue)
        {
            SimulatorProgress.Minimum = 0;
            SimulatorProgress.Maximum = 100;

            SimulatorProgress.Dispatcher.Invoke(() => EwSimulatorProgress.Value = pctValue, DispatcherPriority.Background);
        }

        private void DeleteFile_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!ConfirmWindow.Confirm("Delete deck " + DeckFileList.SelectedItem + "?")) return;

            var writer = new DeckWriter();
            writer.Delete(DeckFileList.SelectedItem as string);
            DeckFileList.ItemsSource = new DeckWriter().GetDeckNames();

            MessageWindow.ShowMessage("Deck has been deleted.");
        }
        
        private void myMainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            CheckForNewVersion();
        }
    }
    
}
