﻿using System.Windows.Controls;
using System.Windows.Shapes;

namespace EkBattleBoff
{
    internal class RepartitionWidgets
    {
        public Rectangle Bar { get; internal set; }
        public Label Label { get; internal set; }
        public Label Percent { get; internal set; }
    }
}