﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.IO.Compression;
using System.Net;

namespace EkBattleBoff
{
    public class Simulator
    {
        public string Version { get; private set; }
        public int Mpm { get; private set; }
        public int Mpf { get; private set; }
        public float AvgRounds { get; private set; }
        public List<Repartition> RoundRepartitions { get; set; }
        public string DeckCooldown { get; private set; }
        public List<Repartition> MpfRepartitions { get; private set; }
        public string ErrorMessage { get; private set; }

        public Simulator()
        {
        }

        public void RunDI(List<CardInstance> deckItems, string demon, int playerLevel, Action<string, int> progressCallback)
        {
            var deckWriter = new DeckWriter();
            deckWriter.Write("temp.txt", deckItems, playerLevel, "temp", true);

            RunSim(new[] { "-dm", demon, "-dk", @"temp\temp.txt" }, progressCallback);
        }

        public void RunEW(List<CardInstance> deckItems, string demon, int playerLevel, Action<string, int> progressCallback)
        {
            var deckWriter = new DeckWriter();
            deckWriter.Write("temp.txt", deckItems, playerLevel, "temp", true);

            RunSim(new[] { "-ew", demon, "-dk", @"temp\temp.txt" }, progressCallback);
        }

        public string GetVersion()
        {
            var args = new string[0];
            RunSim(args, null);
            return Version;
        }

        private string GetVersionNumber()
        {
            var v = GetVersion();
            v = v.Substring(v.IndexOf("(") + 1);
            v = v.Substring(0, v.IndexOf(")"));

            return v;
        }

        public string CheckForUpdates()
        {
            using (var client = new WebClient())
            {
                var downloadsHtml = client.DownloadString("https://bitbucket.org/Crystark/deploy/downloads");
                var url = downloadsHtml.Substring(downloadsHtml.IndexOf("/Crystark/deploy/downloads/ek-battlesim"));
                url = url.Substring(0, url.IndexOf("\""));

                var versionNumber = GetVersionNumber();

                if (!url.Contains(versionNumber)) return "https://bitbucket.org" + url;
            }

            return null;
        }

        private void RunSim(string[] args, Action<string, int> progressCallback)
        {
            ProcessStartInfo startInfo = new ProcessStartInfo();
            startInfo.CreateNoWindow = true;
            startInfo.UseShellExecute = false;
            startInfo.FileName = @"ek-battlesim\ek-battlesim.exe";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.Arguments = "\"" + string.Join("\" \"", args) + "\"";
            startInfo.RedirectStandardOutput = true;

            using (Process exeProcess = Process.Start(startInfo))
            {
                string line;
                var parsingRoundRepartition = false;
                var parsingMpfRepartition = false;
                this.RoundRepartitions = new List<Repartition>();
                this.MpfRepartitions = new List<Repartition>();
                var lineNumber = 0;
                while ((line = exeProcess.StandardOutput.ReadLine()) != null)
                {
                    lineNumber++;
                    if (lineNumber == 3) ErrorMessage = line;
                    if (lineNumber > 3) ErrorMessage = null;

                    if (line.StartsWith("[="))
                    {
                        var percentage = line.Substring(line.LastIndexOf("]") + 2);
                        var pctValue = int.Parse(percentage.Substring(0, percentage.IndexOf("%")));

                        progressCallback(percentage, pctValue);
                    }

                    if(line.StartsWith("Crystark"))
                    {
                        this.Version = line;
                    }

                    var sentinelText = "# Average merit per minute (MPM) |";
                    if (line.StartsWith("# Average"))
                    {
                        // # Average number of rounds       | 65.94 
                        // # Average merit per fight (MPF)  | 20340  
                        // # Average merit per minute (MPM) | 3428  
                        // # Average merit per gem (MPG)    | 339 (Gem ASAP 05:41) 
                        // #                                | 407 (Gem @ 05:00)    
                        // # Deck cooldown                  | 06:04 (152 base cost) 
                        // # --- Round repartition chart
                        // #       25 - 34       |   1.4% 

                        sentinelText = "# Average merit per minute (MPM) |";
                        if(line.StartsWith(sentinelText))
                        {
                            Mpm = int.Parse(line.Substring(sentinelText.Length).Trim());
                        }
                        sentinelText = "# Average merit per fight (MPF)  |";
                        if (line.StartsWith(sentinelText))
                        {
                            Mpf = int.Parse(line.Substring(sentinelText.Length).Trim());
                        }
                        sentinelText = "# Average number of rounds       |";
                        if (line.StartsWith(sentinelText))
                        {
                            AvgRounds = float.Parse(line.Substring(sentinelText.Length).Trim());
                        }
                    }

                    sentinelText = "# Deck cooldown                  |";
                    if (line.StartsWith(sentinelText))
                    {
                        DeckCooldown = line.Substring(sentinelText.Length).Trim();
                    }

                    if (line.StartsWith("# --- Round repartition chart"))
                    {
                        parsingRoundRepartition = true;
                        continue;
                    }
                    if (line.StartsWith("# --- MPF repartition chart"))
                    {
                        parsingMpfRepartition = true;
                        continue;
                    }
                    if (line== "# " || line== "# ---------------------------------------------------------------------------")
                    {
                        parsingRoundRepartition = false;
                        parsingMpfRepartition = false;
                    }
                    if (parsingRoundRepartition)
                    {
                        var label = line.Substring(1);
                        label = label.Substring(0, label.IndexOf("|")).Trim();
                        var percentage = line.Substring(line.IndexOf("|")+1);
                        percentage = percentage.Substring(0, percentage.IndexOf("%") + 1).Trim();
                        var percentValue = double.Parse(percentage.Substring(0, percentage.Length - 1));

                        RoundRepartitions.Add(new Repartition { Label = label, Percentage = percentage, PercentageValue = percentValue });
                    }
                    if(parsingMpfRepartition)
                    {
                        var label = line.Substring(1);
                        label = label.Substring(0, label.IndexOf("|")).Trim();
                        var percentage = line.Substring(line.IndexOf("|") + 1);
                        percentage = percentage.Substring(0, percentage.IndexOf("%") + 1).Trim();
                        var percentValue = double.Parse(percentage.Substring(0, percentage.Length - 1));

                        MpfRepartitions.Add(new Repartition { Label = label, Percentage = percentage, PercentageValue = percentValue });
                    }
                }


            }
        }

        public void DownloadNewVersion(string version)
        {
            using (var client = new WebClient())
            {
                var fileName = version.Substring(version.LastIndexOf("/") + 1);
                Directory.CreateDirectory("temp");
                client.DownloadFile(version, @"temp\" + fileName);

                Directory.Delete("ek-battlesim", true);
                ZipFile.ExtractToDirectory(@"temp\" + fileName, ".");
            }
        }

        public class Repartition
        {
            public string Label { get; internal set; }
            public string Percentage { get; internal set; }
            public double PercentageValue { get; internal set; }
        }
    }
}